Harmonogram:
1. Wykonanie menu
2. wykonanie terenu, tekstur
3. Wykonanie postaci, kolizji, jej poruszania się
4. Wykonanie obiektów kolizyjnych typu drzewa, skały, budynki
5. Implementacja przeciwników
6. Programowanie zasad punktacji, ewentualnej śmierci przeciwnika/ gracza
7. Wykonanie prostego zadania do wykonania przez gracza
8. Dopieszczenie szczegółów, eliminacja błędów, usprawnienia, uzupełnienie animacji